import React from 'react'
import {BrowserRouter as Router ,HashRouter,Switch,Route}  from 'react-router-dom'
import Navbar from './Components/Navbar/Narbar'
const Routers = () => {
    return (
        <div>
            <HashRouter basename="/">
                <Navbar/>
            </HashRouter>
        </div>
    )
}

export default Routers
