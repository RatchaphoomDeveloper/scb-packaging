import React from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import ProgressBar from "../../Utils/Progress";
import "./Home.scss";
const Home = () => {
  const history = useHistory()
  const [tab1, setTab1] = React.useState(true);
  const [tab2, setTab2] = React.useState(false);
  return (
    <div>
      <div className="table-card-container">
        <div
          className="tab-item "
          onClick={() => {
            setTab1(true);
            setTab2(false);
          }}
          id={tab1 === true && "active"}
        >
          <i class="fas fa-poll-h"></i>
          <span>แบบสอบถาม</span>
        </div>
        <div
          className="tab-item"
          onClick={() => {
            setTab1(false);
            setTab2(true);
          }}
          id={tab2 === true && "active"}
        >
          <i class="fas fa-history"></i>
          <span>ประวัติการตอบแบบสอบถาม</span>
        </div>
      </div>
      <div className="tab-body">
        <div
          className="tab-body-items"
          id={tab1 === true && "tab-body-items-active"}
        >
          <div className="card-tab">
            <div className="card-tab-body">
              <img src={process.env.PUBLIC_URL + "/images/layer-4.png"} />
              <div id="flex-body">
                <label onClick={()=>{history.push("/summary?ID=1")}} >
                  แบบสอบถามเพื่อคัดกรองก่อนเข้าพื้นที่
                  <span>หลังวันหยุดวันแรงงานแห่งชาติ 2564</span>
                </label>
                <p>อัตลักษณ์เจลคันถธุระเกสต์เฮาส์ สงบสุขไฟแนนซ์ ตุ๊กตุ๊กแจ็ก</p>
              </div>
            </div>
            <div className="card-tab-footer">
              <div className="card-progress-footer">
                <ProgressBar
                  bgcolor={"#1f6eb4"}
                  bgContainerColor={"#e4e8f0"}
                  leftTime={0}
                  completed={70}
                />
              </div>
              <div className="card-eye-footer">
                <i class="fas fa-eye"></i>
                <p>1,234</p>
              </div>
            </div>
          </div>

          <div className="card-tab">
            <div className="card-tab-body">
              <img src={process.env.PUBLIC_URL + "/images/layer-5.png"} />
              <div id="flex-body">
                <label onClick={()=>{history.push("/summary?ID=2")}}>
                  แบบสอบถามเพื่อคัดกรองก่อนเข้าพื้นที่
                  <span>หลังวันหยุดวันแรงงานแห่งชาติ 2564</span>
                </label>
                <p>อัตลักษณ์เจลคันถธุระเกสต์เฮาส์ สงบสุขไฟแนนซ์ ตุ๊กตุ๊กแจ็ก</p>
              </div>
            </div>
            <div className="card-tab-footer">
              <div className="card-progress-footer">
                <ProgressBar
                  bgcolor={"#f17f80"}
                  bgContainerColor={"#e4e8f0"}
                  leftTime={0}
                  completed={70}
                />
              </div>
              <div className="card-eye-footer">
                <i class="fas fa-eye"></i>
                <p>1,234</p>
              </div>
            </div>
          </div>

          <div className="card-tab">
            <div className="card-tab-body">
              <img src={process.env.PUBLIC_URL + "/images/layer-6.png"} />
              <div id="flex-body">
                <label onClick={()=>{history.push("/summary?ID=3")}}>
                  แบบสอบถามเพื่อคัดกรองก่อนเข้าพื้นที่
                  <span>หลังวันหยุดวันแรงงานแห่งชาติ 2564</span>
                </label>
                <p>อัตลักษณ์เจลคันถธุระเกสต์เฮาส์ สงบสุขไฟแนนซ์ ตุ๊กตุ๊กแจ็ก</p>
              </div>
            </div>
            <div className="card-tab-footer">
              <div className="card-progress-footer">
                <ProgressBar
                  bgcolor={"#e4e8f0"}
                  bgContainerColor={"#e4e8f0"}
                  leftTime={0}
                  completed={70}
                />
              </div>
              <div className="card-eye-footer">
                <i class="fas fa-eye"></i>
                <p>1,234</p>
              </div>
            </div>
          </div>
        </div>

        <div
          className="tab-body-items"
          id={tab2 === true && "tab-body-items-active"}
        >
          <div className="card-tab">
            <div className="card-tab-body">
              <img src={process.env.PUBLIC_URL + "/images/layer-4.png"} />
              <div id="flex-body">
                <label onClick={()=>{history.push("/summary?ID=1")}}>
                  แบบสอบถามเพื่อคัดกรองก่อนเข้าพื้นที่
                  <span>หลังวันหยุดวันแรงงานแห่งชาติ 2564</span>
                </label>
                <p>อัตลักษณ์เจลคันถธุระเกสต์เฮาส์ สงบสุขไฟแนนซ์ ตุ๊กตุ๊กแจ็ก</p>
              </div>
            </div>
            <div className="card-tab-footer">
              <div className="card-progress-footer">
                <ProgressBar
                  bgcolor={"#1f6eb4"}
                  bgContainerColor={"#e4e8f0"}
                  leftTime={0}
                  completed={70}
                />
              </div>
              <div className="card-close-footer">
                <i class="fas fa-times-circle"></i>
                <p>ปิด</p>
              </div>
            </div>
          </div>

          <div className="card-tab">
            <div className="card-tab-body-list">
              <div>
                <i class="fas fa-industry"></i>
                <span>โรงงานที่เข้าปฎิบัติ : <span id="black-span">โรงงานบ้านโป่ง </span></span>
              </div>
              <div>
                <i class="fas fa-calendar-alt"></i>
                <span>
                  วันที่เข้าปฎิบัติ :{" "}
                  <span id="black-span">1 พค. 64 - 6 พค. 64 </span>
                </span>
              </div>
            </div>
          </div>
          <div className="card-tab">
            <div className="card-tab-body-list">
              <div>
                <i class="fas fa-industry"></i>
                <span>โรงงานที่เข้าปฎิบัติ : <span id="black-span">โรงงานมาบตาพุด </span> </span>
              </div>
              <div>
                <i class="fas fa-calendar-alt"></i>
                <span>
                  วันที่เข้าปฎิบัติ :{" "}
                  <span id="black-span">25 เม.ษ. 64 - 30 เม.ษ. 64 </span>
                </span>
              </div>
            </div>
          </div>
          <div className="card-tab">
            <div className="card-tab-body-list">
              <div>
                <i class="fas fa-industry"></i>
                <span>โรงงานที่เข้าปฎิบัติ : <span id="black-span">โรงงานบ้านโป่ง </span></span>
              </div>
              <div>
                <i class="fas fa-calendar-alt"></i>
                <span>
                  วันที่เข้าปฎิบัติ :
                  <span id="black-span">20 เม.ษ. 64 - 24 เม.ษ. 64 </span>
                </span>
              </div>
            </div>
          </div>

          <div className="card-tab">
            <div className="card-tab-body">
              <img src={process.env.PUBLIC_URL + "/images/layer-5.png"} />
              <div id="flex-body">
                <label onClick={()=>{history.push("/summary?ID=2")}}>
                  แบบสอบถามเพื่อคัดกรองก่อนเข้าพื้นที่
                  <span>หลังวันหยุดวันแรงงานแห่งชาติ 2564</span>
                </label>
                <p>อัตลักษณ์เจลคันถธุระเกสต์เฮาส์ สงบสุขไฟแนนซ์ ตุ๊กตุ๊กแจ็ก</p>
              </div>
            </div>
            <div className="card-tab-footer">
              <div className="card-progress-footer">
                <ProgressBar
                  bgcolor={"#f17f80"}
                  bgContainerColor={"#e4e8f0"}
                  leftTime={0}
                  completed={70}
                />
              </div>
              <div className="card-close-footer">
                <i class="fas fa-times-circle"></i>
                <p>ปิด</p>
              </div>
            </div>
          </div>

          <div className="card-tab">
            <div className="card-tab-body">
              <img src={process.env.PUBLIC_URL + "/images/layer-6.png"} />
              <div id="flex-body">
                <label onClick={()=>{history.push("/summary?ID=3")}}>
                  แบบสอบถามเพื่อคัดกรองก่อนเข้าพื้นที่
                  <span>หลังวันหยุดวันแรงงานแห่งชาติ 2564</span>
                </label>
                <p>อัตลักษณ์เจลคันถธุระเกสต์เฮาส์ สงบสุขไฟแนนซ์ ตุ๊กตุ๊กแจ็ก</p>
              </div>
            </div>
            <div className="card-tab-footer">
              <div className="card-progress-footer">
                <ProgressBar
                  bgcolor={"#e4e8f0"}
                  bgContainerColor={"#e4e8f0"}
                  leftTime={0}
                  completed={70}
                />
              </div>
              <div className="card-close-footer">
                <i class="fas fa-times-circle"></i>
                <p>ปิด</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
