import React from 'react'
import {BrowserRouter as Router ,Switch,Route,NavLink}  from 'react-router-dom'
import {useSelector} from 'react-redux'
import Home from '../Home/Home'
import summary from '../summary/Summary'
import Branner from './Branner'
import './Navbar.scss'
import { useHistory } from 'react-router-dom'
const Narbar = (props) => {
    const [clicked,setClicked ] = React.useState(false)
    const history = useHistory()
    const pageData = useSelector(state=>state.page)
    return (
        <div className="bg" >
            <nav className="NavbarItems" >
                <h1 className="navbar-logo" onClick={()=>{history.push("/")}} > 
                SCG PACKAGING
                </h1>
                <div className="menu-icon" onClick={()=>{setClicked(!clicked)}} >
                    <i className={clicked ? "fas fa-times" : "fas fa-bars"} ></i>
                </div>
                <ul className={clicked ? "nav-menu active" : "nav-menu"} >
                    <li>
                        <NavLink to="/" className="nav-links"  >
                        หน้าหลัก
                        </NavLink>
                    </li>
                </ul>
            </nav>
            <div className="branner-container" >
                {pageData === '/' && <Branner/>}
                
            </div>
            <main>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/summary" component={summary} />
                </Switch>
            </main>
        </div>
    )
}

export default Narbar
