import React from 'react'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import './Navbar.scss'
const Branner = () => {
    const settings = {
        dots: false,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 5000,
        rtl: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 2,
              infinite: true,
              dots: true,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2,
            },
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      };
    return (
        <div>
             <Slider {...settings}>
                <div className="carousel-image-container" >
                    <img src={process.env.PUBLIC_URL+"/images/layer-3.png"} />
                </div>
                <div className="carousel-image-container" >
                    <img src={process.env.PUBLIC_URL+"/images/layer-3.png"} />
                </div>

             </Slider>
        </div>
    )
}

export default Branner
