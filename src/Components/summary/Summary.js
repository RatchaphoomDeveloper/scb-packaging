import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import "../Home/Home.scss";
import "./Summary.scss";
const Summary = (props) => {
  const history = useHistory();
  const page_id = new URLSearchParams(props.location.search).get("ID");
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({
      type: "set",
      page: "/summary",
    });
    return () => {};
  }, []);
  return (
    <div>
      {page_id === "1" && (
        <div>
          <div className="card-tab">
            <div className="card-tab-body">
              <img src={process.env.PUBLIC_URL + "/images/layer-4.png"} />
              <div id="flex-body">
                <label
                  onClick={() => {
                    history.push("/summary?ID=1");
                  }}
                >
                  แบบสอบถามเพื่อคัดกรองก่อนเข้าพื้นที่
                  <span>หลังวันหยุดวันแรงงานแห่งชาติ 2564</span>
                </label>
                <p>อัตลักษณ์เจลคันถธุระเกสต์เฮาส์ สงบสุขไฟแนนซ์ ตุ๊กตุ๊กแจ็ก</p>
              </div>
            </div>
            <div className="card-tab-footer-hr"></div>
            <div className="card-tab-footer-summary">
              <p>นายจิตมั่น ตั้งใจดี </p>
              <div className="card-tab-footer-flex">
                <p>
                  หน่วยงาน : <span>โบ้ยบาร์บี้ฟอร์มเอสเปรสโซวีเจ</span>
                </p>
                <p>
                  แผนก : <span>โบ้ยบาร์บี้ฟอร์มเอสเปรสโซวีเจ</span>
                </p>
              </div>
            </div>
          </div>
          <div className="half-circle"></div>
          <div className="summary-body">
            <img
              id="img-covidman"
              src={process.env.PUBLIC_URL + "/images/risk-icon-01.png"}
            />
            <h1>ความเสี่ยงต่ำ</h1>
            <h2>(81 คะแนน)</h2>
            <p id="header">สามารถดูแลตัวเองเบื้องต้นได้ดังนี้</p>
            <div className="summary-body-content">
              <p>1. หมั่นล้างมือด้วยสบู่หรือแอลกอฮอล์</p>
              <p>2. สวมหน้ากากอนามัย</p>
              <p>3. หลีกเลี่ยงสถานที่แออัด</p>
            </div>
            <div className="summary-qr-code">
              <img
                id={"img-qrcode"}
                src={process.env.PUBLIC_URL + "/images/vector-smart-object.png"}
              />
            </div>
          </div>
          <div className="card-tab-footer-hr2"></div>
          <div className="summary-body-footer">
            <p id={"solid-1"}>
              <span className="fas fa-times-circle"></span>
              Close
            </p>
            <p id={"solid-2"}>
              <span className="fas fa-download"></span>
              Close
            </p>
          </div>
        </div>
      )}
      {page_id === "2" && (
        <div>
          <div className="card-tab">
            <div className="card-tab-body">
              <img src={process.env.PUBLIC_URL + "/images/layer-5.png"} />
              <div id="flex-body">
                <label
                  onClick={() => {
                    history.push("/summary?ID=2");
                  }}
                >
                  แบบสอบถามเพื่อคัดกรองก่อนเข้าพื้นที่
                  <span>หลังวันหยุดวันแรงงานแห่งชาติ 2564</span>
                </label>
                <p>อัตลักษณ์เจลคันถธุระเกสต์เฮาส์ สงบสุขไฟแนนซ์ ตุ๊กตุ๊กแจ็ก</p>
              </div>
            </div>
            <div className="card-tab-footer-hr"></div>
            <div className="card-tab-footer-summary">
              <p>นายจิตมั่น ตั้งใจดี </p>
              <div className="card-tab-footer-flex">
                <p>
                  หน่วยงาน : <span>โบ้ยบาร์บี้ฟอร์มเอสเปรสโซวีเจ</span>
                </p>
                <p>
                  แผนก : <span>โบ้ยบาร์บี้ฟอร์มเอสเปรสโซวีเจ</span>
                </p>
              </div>
            </div>
          </div>
          <div className="half-circle"></div>
          <div className="summary-body">
            <img
              id="img-covidman"
              src={process.env.PUBLIC_URL + "/images/risk-icon-02.png"}
            />
            <h1 style={{ color: "#b9932d" }}>ความเสี่ยงต่ำ</h1>
            <h2 style={{ color: "#b9932d" }}>(15 คะแนน)</h2>
            <p id="header">สามารถดูแลตัวเองเบื้องต้นได้ดังนี้</p>
            <div className="summary-body-content">
              <p>1. หมั่นล้างมือด้วยสบู่หรือแอลกอฮอล์</p>
              <p>2. สวมหน้ากากอนามัย</p>
              <p>3. หลีกเลี่ยงสถานที่แออัด</p>
            </div>
            <div className="summary-qr-code">
              <img
                id={"img-qrcode"}
                src={process.env.PUBLIC_URL + "/images/vector-smart-object.png"}
              />
            </div>
          </div>
          <div className="card-tab-footer-hr2"></div>
          <div className="summary-body-footer">
            <p id={"solid-1"}>
              <span className="fas fa-times-circle"></span>
              Close
            </p>
            <p id={"solid-2"}>
              <span className="fas fa-download"></span>
              Close
            </p>
          </div>
        </div>
      )}
      {page_id === "3" && (
        <div>
          <div className="card-tab">
            <div className="card-tab-body">
              <img src={process.env.PUBLIC_URL + "/images/layer-6.png"} />
              <div id="flex-body">
                <label
                  onClick={() => {
                    history.push("/summary?ID=3");
                  }}
                >
                  แบบสอบถามเพื่อคัดกรองก่อนเข้าพื้นที่
                  <span>หลังวันหยุดวันแรงงานแห่งชาติ 2564</span>
                </label>
                <p>อัตลักษณ์เจลคันถธุระเกสต์เฮาส์ สงบสุขไฟแนนซ์ ตุ๊กตุ๊กแจ็ก</p>
              </div>
            </div>
            <div className="card-tab-footer-hr"></div>
            <div className="card-tab-footer-summary">
              <p>นายจิตมั่น ตั้งใจดี </p>
              <div className="card-tab-footer-flex">
                <p>
                  หน่วยงาน : <span>โบ้ยบาร์บี้ฟอร์มเอสเปรสโซวีเจ</span>
                </p>
                <p>
                  แผนก : <span>โบ้ยบาร์บี้ฟอร์มเอสเปรสโซวีเจ</span>
                </p>
              </div>
            </div>
          </div>
          <div className="half-circle"></div>
          <div className="summary-body">
            <img
              id="img-covidman"
              src={process.env.PUBLIC_URL + "/images/risk-icon-03.png"}
            />

            <h1 style={{ color: "#e73571" }}>ความเสี่ยงต่ำ</h1>
            <h2 style={{ color: "#e73571" }}>(1 คะแนน)</h2>
            <p id="header">สามารถดูแลตัวเองเบื้องต้นได้ดังนี้</p>
            <div className="summary-body-content">
              <p>1. หมั่นล้างมือด้วยสบู่หรือแอลกอฮอล์</p>
              <p>2. สวมหน้ากากอนามัย</p>
              <p>3. หลีกเลี่ยงสถานที่แออัด</p>
            </div>
            <div className="summary-qr-code">
              <img
                id={"img-qrcode"}
                src={process.env.PUBLIC_URL + "/images/vector-smart-object.png"}
              />
            </div>
          </div>
          <div className="card-tab-footer-hr2"></div>
          <div className="summary-body-footer">
            <p id={"solid-1"}>
              <span className="fas fa-times-circle"></span>
              Close
            </p>
            <p id={"solid-2"}>
              <span className="fas fa-download"></span>
              Close
            </p>
          </div>
        </div>
      )}
    </div>
  );
};

export default Summary;
