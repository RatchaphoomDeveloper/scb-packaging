import React from "react";
import "./Progress.scss";
const ProgressBar = (props) => {
  const { bgcolor, bgContainerColor, completed, leftTime } = props;
  const containerStyles = {
    backgroundColor: bgContainerColor,
    borderRadius: 50,
  };

  const fillerStyles = {
    height: "100% ",
    width: `${completed}%`,
    maxWidth: "100%",
    backgroundColor: bgcolor,
    borderRadius: "inherit",
    textAlign: "left",
    paddingLeft: "5px",
    display: "flex",
    alignItems: "center",
  };

  const labelStyles = {
    // padding: 5,
    color: "white",
    fontWeight: "bold",
    whiteSpace: "nowrap",
    fontSize: 16,
    marginLeft: 10,
  };

  return (
    <div style={containerStyles} className="progress-containerStyles">
      <div style={fillerStyles}>
        <span style={labelStyles}></span>
      </div>
    </div>
  );
};

export default ProgressBar;
