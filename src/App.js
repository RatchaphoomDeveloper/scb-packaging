import Routers from './Routers'
import './App.scss';

function App() {
  return (
    <div>
        <Routers/>
    </div>
  );
}

export default App;
